import { settings } from './SettingsService'

const API_SERVER = settings.API_SERVER;

export const api_routes = {
    auth: {
        login: API_SERVER + 'login',
        logout: API_SERVER + 'logout',
        register: API_SERVER + 'register',
        refresh_token: API_SERVER + 'refresh_token'
    },
    mail: {
        sendMail: API_SERVER + 'mailable'
    }

}