import axios from 'axios'
import { settings } from './SettingsService'

const API_SERVER = settings.API_SERVER;

const apiClient = axios.create({
    baseURL: API_SERVER,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    }
})

export default {
    login(credentials) {
        return apiClient.post('/login', credentials)
    }
}