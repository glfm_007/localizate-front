let api;


if (process.env.NODE_ENV === 'production')
    api = 'https://cors-anywhere.herokuapp.com/http://159.89.91.255:9090/api/auth';
else {
    api = 'http://127.0.0.1:8001/api/auth/'
}

export const settings = {
    APP_NAME: 'localizate-ya',
    API_SERVER: api
}