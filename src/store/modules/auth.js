import axios from 'axios';
import { globalStore } from "../../main";
import { api_routes } from '../../services/ApiService';
import AuthService from '@/services/AuthService.js';

const state = {
    user: null
}

const actions = {

    login({ commit, dispatch }, credentials) {
        return AuthService.login(credentials).then((data) => {
            commit('SET_USER', data);

            dispatch('addNotification', {
                type: 'success',
                icon: 'mdi-account',
                message: `Bienvenido ${data.data.user.name}`
            }, { root: true })
        })
        .catch(error => {
            dispatch('addNotification', {
                type: 'error',
                icon: 'close-octagon',
                message: 'Sus credenciales no coinciden, favor intente nuevamente.'
            }, { root: true })
            throw error
        });
    },

    register({ commit, dispatch }, credentials) {
        return axios.post(api_routes.auth.register, credentials).then(
            ({ data }) => {
                commit('SET_USER', data)

                dispatch('addNotification', {
                    type: 'success',
                    icon: 'mdi-account',
                    message: 'Te has registrado correctamente!'
                }, { root: true })
            }
        )
        .catch(error => {
            dispatch('addNotification', {
                type: 'error',
                icon: 'close-octagon',
                message: 'Ha ocurrido un error al registrarse: ' + error.message
            }, { root: true })
            throw error
        });
    },

    logout({ commit, dispatch }) {
        commit('REMOVE_USER')

        dispatch('addNotification', {
            type: 'success',
            icon: '',
            message: 'Ha cerrado sesion correctamente!'
        }, { root: true })
    }
}

const mutations = {
    SET_USER: (state, userData) => {
        state.user = userData;
        globalStore.globalUser = userData;
        localStorage.setItem('user', JSON.stringify(userData))
    },
    REMOVE_USER: (state) => {
        localStorage.removeItem('user')
        globalStore.globalUser = null;
    }
}


export default {
    state,
    actions,
    mutations,
}