const state = {
    notifications: []
}

const actions = {
    addNotification({ commit }, notification) {
        commit('PUSH_NOTIFICATION', notification);
    }
}

const mutations = {
    PUSH_NOTIFICATION: (state, notification) => {
        state.notifications.push({
            ...notification,
            id: (Math.random().toString(36)).substr(2)
        })
    }
}



export default {
    state,
    actions,
    mutations
}