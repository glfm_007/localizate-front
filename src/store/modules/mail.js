import axios from 'axios';
import { api_routes } from '../../services/ApiService';

const state = {
    mail: null
}

const actions = {

    sendMail({ commit }, data) {
        console.log(data);
        return axios.post(api_routes.mail.sendMail, data).then(({ data }) => {
            commit('SET_EMAIL_DATA', data);
        })
    }
}

export default {
    state,
    actions
}