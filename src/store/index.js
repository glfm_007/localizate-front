import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './modules/auth'
import Mail from './modules/mail'
import Notification from "./modules/notification"

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: Auth,
    mail: Mail,
    notification: Notification
  }
})
