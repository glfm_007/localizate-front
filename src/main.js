import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCv5A3vzX4Tzgh8TkYu41zYS9A-9lk83jM",
    libraries: "places" // necessary for places input
  }
});

 export const globalStore = new Vue({
  data: {
    globalUser: JSON.parse(localStorage.getItem('user'))
  }
})


Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
